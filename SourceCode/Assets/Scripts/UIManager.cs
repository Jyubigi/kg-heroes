﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using UnityEngine.UI;
public class UIManager : MonoBehaviour {
    public List<Animator> UIs = new List<Animator>();
    public UnjuStar Unju = null;
    public MeteorCreater Meteo = null;
    public SSamManager ssamMgr = null;
    public Text Score = null;
    int sco = 0;
    public void Safe()
    {
        ++sco;
        Score.text = sco.ToString();
    }
    public void OuttaGame()//ui 치우기
    {
        foreach (Animator ui in UIs)
        {
            ui.SetTrigger("outtaGame");
        }
        Meteo.StartMeteos();
        Unju.StartTeacher();
        Score.text = "0";
        sco = 0;
    }
    public void IntoGame()//ui 끌어오기
    {
        if(ssamMgr.ind == 0)
        {
            SoundMgr.Instance.Play_Sound("GameOver_Rider");
        }
        else if (ssamMgr.ind == 1)
        {
            SoundMgr.Instance.Play_Sound("GameOver_Runner");
        }
        else if (ssamMgr.ind == 2)
        {
            SoundMgr.Instance.Play_Sound("GameOver_Flitter");
        }
        Unju.StopTeacher();
        Meteo.StopMeteos();
        foreach (Animator ui in UIs)
        {
            ui.SetTrigger("intoGame");
        }
    }
}
