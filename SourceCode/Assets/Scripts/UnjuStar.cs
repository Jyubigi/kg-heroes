﻿using UnityEngine;
using System.Collections;

public class UnjuStar : MonoBehaviour
{
    public float SpinSpeed_max = 1f;
    public float SpinSpeed_min = 0.5f;
    float spinSpeed = 0.5f;
    float buttonTime = 0f;
    float spinDirec = -1f;
    float stopAngle = 0f;

    bool ChangeDir = false;

    void Awake()
    {
        StartCoroutine(Spin());

    }
    public void StopTeacher()
    {
        spinDirec = 0f;
    }
    public void StartTeacher()
    {
        spinDirec = -1f;
    }
    IEnumerator Spin()
    {
        spinSpeed = SpinSpeed_min;
        while (true)
        {
            if (Input.GetMouseButton(0))
            {
                if (!ChangeDir)
                {
                    spinDirec *= -1f;
                    spinSpeed = 1f;
                    buttonTime = 0f;
                    ChangeDir = true;
                }
                spinSpeed = SpinSpeed_max;
                transform.eulerAngles = new Vector3(0f, 0f, transform.eulerAngles.z + spinSpeed * spinDirec * Time.deltaTime);
            }
            else
            {
                ChangeDir = false;
                spinSpeed = Mathf.Lerp(SpinSpeed_max, SpinSpeed_min, buttonTime);
                buttonTime += Time.deltaTime * 3;
                transform.eulerAngles = new Vector3(0f, 0f, transform.eulerAngles.z + spinSpeed * spinDirec * Time.deltaTime);
            }
            yield return new WaitForFixedUpdate();
        }
    }
}
