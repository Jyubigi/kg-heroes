﻿using UnityEngine;
using System.Collections;

public class MeteorCreater : MonoBehaviour
{
    float cd = 0.0f;
    public float createDelay = 0.0f;
    public Transform pool = null;
    public Meteor[] meteors = new Meteor[10];
    public bool isStop = false;

    void Awake()
    {
        for (int i = 0; i < 8; i++)
        {
            meteors[i].gameObject.SetActive(false);
            meteors[i].SetFirst();
        }
    }

    void Update()
    {
        if(Input.GetKey(KeyCode.Escape))
        {

        }

        if (isStop)
            return;
        cd -= Time.deltaTime/3;
        int r;
        if (cd < 0)
        {
            for (int i = 0; i < 8; i++)
            {
                r = Random.Range(0, 7);
                if (!meteors[r].gameObject.activeSelf)
                {
                    MeteorSetting(r);
                    break;
                }
            }
            cd = Random.Range(cd, cd + 0.5f);
        }
    }


    void MeteorSetting(int _index)
    {
        
        meteors[_index].gameObject.SetActive(true);
        meteors[_index].StartMeteor();
    }
    public void StopMeteos()
    {
        isStop = true;
        for (int i = 0; i < 8; i++)
        {
            meteors[i].StopMeteor();
        }
    }
    public void StartMeteos()
    {
        isStop = false;
        for (int i = 0; i < 8; i++)
        {
            meteors[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < 8; i++)
        {
            meteors[i].StartMeteor();
        }
    }
}