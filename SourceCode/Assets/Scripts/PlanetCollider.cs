﻿using UnityEngine;
using System.Collections;

public class PlanetCollider : MonoBehaviour {
    public UIManager Ui = null;
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag.Equals("Meteor"))
            Ui.IntoGame();
    }
}