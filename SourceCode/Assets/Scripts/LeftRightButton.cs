﻿using UnityEngine;
using System.Collections;

public class LeftRightButton : MonoBehaviour {
    public SSamManager SSam = null;
    public void Left()
    {
        SSam.Left();
    }
    public void Right()
    {
        SSam.Right();
    }
}
