﻿using UnityEngine;
using System.Collections;

public class Escape : MonoBehaviour {

    public GameObject escape = null;
    public GameObject[] Etc = new GameObject[0];
    public MeteorCreater meteor = null;
	
	void Update () {
	    if(Input.GetKey(KeyCode.Escape) && meteor.isStop)
        {
            Etc[0].SetActive(false);
            Etc[1].SetActive(false);
            escape.SetActive(true);
        }
	}

    public void NoExit()
    {
        Etc[0].SetActive(true);
        Etc[1].SetActive(true);
        escape.SetActive(false);
    }
}
