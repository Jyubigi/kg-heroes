﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Button : MonoBehaviour {

    public Image image = null;
    public Sprite mute = null;
    public Sprite play = null;
    public GameObject credit = null;
	
    public void Click()
    {
        SoundMgr.Instance.Off_Sound();

        if (SoundMgr.Instance.SoundState)
            image.sprite = play;
        else
            image.sprite = mute;
    }

    public void Credit()
    {   
        credit.SetActive(true);
    }

    public void Creditexit()
    {
        credit.SetActive(false);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
