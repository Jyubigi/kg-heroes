﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class SSamManager : MonoBehaviour {

    public GameObject[] Ssam = new GameObject[3];

    public int ind = 0;

    public void Left()
    {

        --ind;
        if (ind < 0)
            ind = 2;

        if(ind == 0)
        {
            SoundMgr.Instance.Play_Sound("Select_Rider");
        }
        else if(ind == 1)
        {
            SoundMgr.Instance.Play_Sound("Select_Runner");
        }
        else if(ind == 2)
        {
            SoundMgr.Instance.Play_Sound("Select_Flitter");
        }

        for (int i = 0; i < 3; i++)
            Ssam[i].SetActive(false);
        Ssam[ind].SetActive(true);
    }
    public void Right()
    {
        ++ind;
        if (ind > 2)
            ind = 0;

        if (ind == 0)   
        {
            SoundMgr.Instance.Play_Sound("Select_Rider");
        }
        else if (ind == 1)
        {
            SoundMgr.Instance.Play_Sound("Select_Runner");
        }
        else if (ind == 2)
        {
            SoundMgr.Instance.Play_Sound("Select_Flitter");
        }

        for (int i = 0; i < 3; i++)
            Ssam[i].SetActive(false);
        Ssam[ind].SetActive(true);
    }
}
