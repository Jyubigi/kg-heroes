﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Teacher : MonoBehaviour
{
    public int index = 0;
    public UIManager ui = null;
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.CompareTag("Meteor"))//사운드 재생할 것!
        {
            if (index == 0) // 라이더
            {
                if (Random.Range(0, 2) > 0)
                {
                    SoundMgr.Instance.Play_Sound("Hit1_Rider");
                }
                else
                {
                    SoundMgr.Instance.Play_Sound("Hit2_Rider");
                }
            }
            else if (index == 1) // 러너
            {
                SoundMgr.Instance.Play_Sound("Hit_Runner");
            }
            else if (index == 2)
            {
                SoundMgr.Instance.Play_Sound("Hit_Flitter");
            }
            ui.Safe();
        }
    }

}