﻿using UnityEngine;
using System.Collections;

public class SSamMemo : MonoBehaviour {

    public GameObject[] Ssam = new GameObject[3];
    int ind = 0;
    public void Left()
    {
        --ind;
        if (ind < 0)
            ind = 2;
        for (int i = 0; i < 3; i++)
            Ssam[i].SetActive(false);
        Ssam[ind].SetActive(true);
    }
    public void Right()
    {
        ++ind;
        if (ind > 2)
            ind = 0;
        for (int i = 0; i < 3; i++)
            Ssam[i].SetActive(false);
        Ssam[ind].SetActive(true);
    }
}
