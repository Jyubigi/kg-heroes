﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundMgr : MonoBehaviour {

    private static SoundMgr instance = null;

    public static SoundMgr Instance
    {
        get
        {
            if (instance == null)
            {

            }
            return instance;
        }
    }

    public List<AudioSource> soundList = new List<AudioSource>();
    public bool SoundState = true;

	void Awake () {
        instance = this as SoundMgr;
	}

    public void Play_Sound(string _sName)
    {
        if (!SoundState) return;

        for (int i = 0; i < soundList.Count; i++)
        {
            if (soundList[i].clip.name == _sName)
            {
                soundList[i].Play();
            }
        }
    }

    public void Off_Sound()
    {
        if (SoundState)
        {
            for (int i = 0; i < soundList.Count; i++)
            {
                soundList[i].Stop();
            }
            SoundState = false;
        }
        else if (!SoundState)
        {

            for (int i = 0; i < soundList.Count; i++)
            {
                if (soundList[i].playOnAwake)
                {
                    soundList[i].Play();
                }
            }
            SoundState = true;
        }
    }
}
