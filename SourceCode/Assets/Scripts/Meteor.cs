﻿using UnityEngine;
using System.Collections;

public class Meteor : MonoBehaviour {
    public Animator anim = null;
    public float MinSpeed = 1f;
    float digree = 0.0f;
    float speed = 1f;
    bool isStop = false;
    public Vector3 firstPos = Vector3.zero;

    public void SetFirst()
    {
        firstPos = transform.position;
        speed = MinSpeed;
    }
    void Start() {
        
        anim = transform.GetComponent<Animator>();
    }

    void Update() {
        if (!isStop)
        {
            float digreeX = Mathf.Cos(digree);
            float digreeY = Mathf.Sin(digree);
            transform.localPosition += new Vector3(digreeX * Time.deltaTime * speed, digreeY * Time.deltaTime * speed, 0);
        }

    }
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag.Equals("Player"))
        {
            anim.SetTrigger("boom");
            GetComponent<CircleCollider2D>().enabled = false;
        }
        else if (coll.tag.Equals("planet"))
        {
            anim.SetTrigger("coll");
            StopMeteor();
        }
    }
    public void StopMeteor()
    {
        isStop = true;
    }
    public void StartMeteor()
    {
        transform.position = firstPos;
        isStop = false;
        GetComponent<CircleCollider2D>().enabled = true;

        digree = Mathf.Atan2(0 - transform.localPosition.y, 0 - transform.localPosition.x);

        var angle = Mathf.Atan2(transform.localPosition.y, transform.localPosition.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
    }
    public void Fade()
    {
        gameObject.SetActive(false);
    }
    public void BangStart()
    {
        transform.localScale = new Vector3(0.6f, 0.6f, 1f);
    }
    public void BangEnd()
    {
        speed = MinSpeed;
        transform.localScale = new Vector3(0.3f, 0.3f, 1f);
    }
}